#!/usr/bin/python
#-*- coding: utf-8 -*-

import numpy as np
import threading
import time
from datetime import datetime
import jderobot
import math
import cv2
from math import pi as pi
import random

time_cycle = 80
spiral_value = 0

class MyAlgorithm(threading.Thread):

    def __init__(self, pose3d, motors, laser, bumper):
        self.pose3d = pose3d
        self.motors = motors
        self.laser = laser
        self.bumper = bumper

        self.stop_event = threading.Event()
        self.kill_event = threading.Event()
        self.lock = threading.Lock()
        threading.Thread.__init__(self, args=self.stop_event)


    def parse_laser_data(self,laser_data):
        laser = []
        for i in range(180):
            dist = laser_data.values[i]
            angle = math.radians(i)
            laser += [(dist, angle)]
        return laser

    def laser_vector(self,laser_array):
        laser_vectorized = []
        for d,a in laser_array:
            x = d * math.cos(a) * -1
            y = d * math.sin(a) * -1
            v = (x, y)
            laser_vectorized += [v]
        return laser_vectorized

    def run (self):
        while (not self.kill_event.is_set()):

            start_time = datetime.now()

            if not self.stop_event.is_set():
                self.execute()

            finish_Time = datetime.now()

            dt = finish_Time - start_time
            ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
            #print (ms)
            if (ms < time_cycle):
                time.sleep((time_cycle - ms) / 1000.0)

    def stop (self):
        self.motors.sendV(0)
        self.motors.sendAZ(0)
        self.stop_event.set()

    def play (self):
        if self.is_alive():
            self.stop_event.clear()
        else:
            self.start()

    def kill (self):
        self.kill_event.set()

    def laser_range_mean(self, start_value, end_value, total):
        value_sum = 0
        laser_data = self.laser.getLaserData()
        laser_data=self.parse_laser_data(laser_data)
        laser_data=np.array(laser_data,dtype=float)
        for i in range (start_value,end_value):
            value_sum=value_sum+laser_data[i][0]
        return value_sum/total

    def spiral_motion(self):
        """
            # ini belom dipake #
            ini harus coba2, intinya misal kasih kecepatan angular 8,
            dia harus mengecil terus setiap eksekusi misal 8, 7.5, 6, ....
            supaya dia kyk spiral, ada sumber yg pake e^-n, otomatis dia
            menurun mendekati 0 atau nanti lurus sendiri
        """
        global spiral_value
        spiral_value = spiral_value + 1
        w = 8*math.exp(-spiral_value*0.01)
        print("w=", w)
        self.motors.sendW(w)
        self.motors.sendV(1)

    def rotate_motion(self, rotate_duration, angular_vel):
        """
            rotasi vacuum cleaner, kasih kecepatan angular, setelah itu
            harus kasih sleep biar ngasih waktu dia muter berapa derajatnya,
            kalo udah selesai kasih kecepatan angular 0 lagi supaya berenti,
            terus sleep diakhir biar nggk bug
        """
        self.motors.sendVX(0)
        self.motors.sendAZ(angular_vel)
        time.sleep(rotate_duration)
        self.motors.sendAZ(0)
        time.sleep(0.3)

    def move_forward_motion(self, linier_vel):
        """
            pastiin kecepatan angularnya 0 dulu, baru dikasih kecepatan linier
            supaya nggk belok
        """
        self.motors.sendW(0)
        self.motors.sendV(linier_vel)

    def execute(self):
        print('Execute')
        ANGULAR_VEL_MAX = 3
        LINIER_VEL_MAX = 1
        ROTATE_TIME = np.random.uniform(0.2, 0.6)

        """
        sumber wiki JDERobot sama youtube
        Jadi tuh laser itu cover lingkaran 180 derajat di depan vacuum cleaner
        buat cek tabrakan bagi 3 bagian, nah kalo di ROS bisa minta beberapa value
        template bagi 180 titik value, jadi asumsi tiap 1 derajat titik ada value,
        nah itu jadi ada bagian kanan, tengah, kiri, buat ngecek mana yg paling mau nabrak
        pake mean aja, nah rangenya nggk bagi rata, ada yg titik diilangin,
        kyknya biar keliatan mana bagian2nya, tinggal coba aja kalo bagi rata
        """
        right_mean = self.laser_range_mean(0, 20, 20)
        middle_mean = self.laser_range_mean(75, 105, 30)
        left_mean = self.laser_range_mean(160, 180, 20)
        print(right_mean)
        print(middle_mean)
        print(left_mean)

        """
        Abis diitung mean-nya, cek nabrak atau kg, kalo kg cek lagi ada yg hampir
        nabrak atau kg, ini konstanta 0.3 sam 0.2 harus masih dicoba2 ini masih asal
        abis itu tinggal puter2
        """
        if self.bumper.getBumperData().state == 0:
            if right_mean < 0.4 and left_mean < 0.4 and middle_mean < 0.3:
                print("kg nabrak => collision semuanya")
                self.move_forward_motion(-LINIER_VEL_MAX)
                time.sleep(1)

            elif right_mean < 0.4:
                print("kg nabrak => collision kanan")
                self.rotate_motion(ROTATE_TIME, -ANGULAR_VEL_MAX)

            elif left_mean < 0.4:
                print("kg nabrak => collision kiri")
                self.rotate_motion(ROTATE_TIME, ANGULAR_VEL_MAX)

            elif middle_mean < 0.3:
                print("kg nabrak => collision tengah")
                if right_mean > left_mean:
                    self.rotate_motion(ROTATE_TIME, ANGULAR_VEL_MAX)
                else:
                    self.rotate_motion(ROTATE_TIME, -ANGULAR_VEL_MAX)

            else:
                print("kg nabrak => lurus aja")
                self.move_forward_motion(LINIER_VEL_MAX)

        else:
            """
            else ketika nabrak, masih harus coba2, suka nyangkut
            """
            if right_mean > left_mean:
                print("nabrak => collision kanan")
                self.rotate_motion(ROTATE_TIME, 2*ANGULAR_VEL_MAX)

            elif right_mean < left_mean:
                print("nabrak => collision kiri")
                self.rotate_motion(ROTATE_TIME, -2*ANGULAR_VEL_MAX)
